const express = require('express');
const Bodyparser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');

const app = express();
app.set("view engine","ejs");

app.use(express.urlencoded({extended:true}));

const staticPath = path.join(__dirname, "./public");
app.use(express.static(staticPath)); //middleware

mongoose.connect("mongodb://localhost:27017/fooduser");

const registerSchema = new mongoose.Schema({
    Name : String,
    Email : String,
    Mobile : Number,
    Password : String
});

const registerCollection = mongoose.model("register",registerSchema);

var loginmsg="";
var registermsg="";
app.get("/", function (req, res) {
    res.send("hey");
});

app.get("/login",function(req,res){
    res.render("login",{ejslogin : loginmsg});
})

app.get("/register",function(req,res){
    res.render("register",{ejsregister : registermsg});
});


app.post("/login",function(req,res){
   var name = req.body.nm;
   var pass= req.body.pwd;
   registerCollection.findOne({Email: name},function(err,data){
    if(err){
        console.log(err);
      
    }
    else{
        if(data){
            if(data.Password === pass){
                loginmsg= "Login Succesful!";
                console.log("redirecting to order page");
                res.render("order");
                loginmsg="";
            }
            else{
                loginmsg="User Not Found!";
            }
        }
       
    }
   });
  
});



app.post("/register",function(req,res){
    var email = req.body.mail;
    var pass = req.body.pas;
    var regname = req.body.name;
    var mobile = req.body.mob;
    const user = new registerCollection({
       Name : regname,
       Email : email,
       Mobile : mobile,
       Password :pass
    });
    user.save();
    registermsg="Registration Successful";
    console.log("registration successful!");
   
    registermsg="";
 

});

app.listen(5000, function () {
    console.log("server started");
})